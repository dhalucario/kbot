use serde::{Serialize, Deserialize};
use teloxide::types::{ChatId, MessageId, UserId};
use teloxide::utils::command::BotCommands;
use std::io::ErrorKind;
use std::collections::HashMap;
use std::default::Default;

const ADMINS: [UserId; 1] = [
    UserId(144778621)
];

pub enum IdOrData<I, D> {
    Id(I),
    Data(D)
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum EventData {
    JoinToggle {event_id: String},
    AddGuest {event_id: String},
    RemoveGuest {event_id: String}
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase")]
pub enum Command {
    Post(String),
    Help,
    Hilfe,
    DSGVO,
    Reload
}

const CACHE_FILE_PATH: &str = "./cache.json";

#[derive(Serialize, Deserialize)]
pub struct Cache {
    pub last_posts: HashMap<ChatId, MessageId>,
}

impl Default for Cache {
    fn default() -> Self {
        Self {
            last_posts: Default::default()
        }
    }
}

pub fn user_is_admin(id: &UserId) -> bool {
    ADMINS.contains(id)
}

pub fn load_cache() -> anyhow::Result<Cache> {
    let file_result = std::fs::File::options()
        .read(true)
        .create(false)
        .open(CACHE_FILE_PATH);

    match file_result {
       Ok(file) => {
           Ok(serde_json::from_reader(file)?)
       }
        Err(err) => {
            match err.kind() {
                ErrorKind::NotFound => {
                    let new_file = std::fs::File::options()
                        .create(true)
                        .write(true)
                        .open(CACHE_FILE_PATH)?;
                    let new_data = Cache::default();

                    serde_json::to_writer_pretty(new_file, &new_data)?;
                    Ok(new_data)
                }
                _ => {
                    Err(err.into())
                }
            }
        }
    }
}

pub fn save_cache(cache: &Cache) -> anyhow::Result<()> {
    let file = std::fs::File::options()
        .write(true)
        .create(false)
        .truncate(true)
        .open(CACHE_FILE_PATH)?;

    Ok(serde_json::to_writer(file, cache)?)
}

pub fn save_last_post_message_id(chat_id: ChatId, message_id: MessageId) -> anyhow::Result<()> {
    let mut cache = load_cache()?;
    let _ = cache.last_posts.insert(chat_id, message_id);
    Ok(save_cache(&cache)?)
}

pub fn get_last_post_message_id(chat_id: ChatId) -> anyhow::Result<Option<MessageId>> {
    let cache = load_cache()?;
    if let Some(message_id) = cache.last_posts.get(&chat_id) {
        Ok(Some(message_id.to_owned()))
    } else {
        Ok(None)
    }
}

pub fn delete_last_post_message_id(chat_id: ChatId) -> anyhow::Result<Option<MessageId>> {
    let mut cache = load_cache()?;
    let last_post = cache.last_posts.remove(&chat_id);
    save_cache(&cache)?;
    Ok(last_post)
}