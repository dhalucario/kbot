#[macro_use]
extern crate dotenv_codegen;
#[macro_use]
extern crate lazy_static;

mod common;
mod pb;
mod handlers;
mod templates;

use dotenv::dotenv;
use log::info;
use teloxide::prelude::*;
use handlers::*;

const TELEGRAM_TOKEN: &str = dotenv!("TELEGRAM_TOKEN");

#[tokio::main]
async fn main() {
    env_logger::init();
    info!("Starting bot!");
    dotenv().expect("Could not load dotenv");
    let bot = Bot::new(TELEGRAM_TOKEN);

    let handler_tree = dptree::entry()
        .branch(Update::filter_message().endpoint(on_message))
        .branch(Update::filter_inline_query().endpoint(on_inline_query))
        .branch(Update::filter_callback_query().endpoint(on_callback_query));

    Dispatcher::builder(bot, handler_tree)
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
}