use log::{error, warn};
use crate::common::{EventData, Command, user_is_admin, save_last_post_message_id, get_last_post_message_id, delete_last_post_message_id};
use teloxide::{utils::command::BotCommands, types::{InlineKeyboardButton, InlineKeyboardMarkup}, prelude::*};
use crate::pb::*;
use crate::templates::{get_post_text, update_post};

pub async fn on_message(bot: Bot, msg: Message) -> ResponseResult<()> {
    let me = bot.get_me().await?;
    if let Some(msg_text) = msg.text() {
        if let Ok(cmd) = Command::parse(msg.text().unwrap(), me.username()) {
            match cmd {
                Command::Help | Command::Hilfe => {
                    on_help(&bot, &msg).await
                },
                Command::DSGVO => {
                    on_dsgvo(&bot, &msg).await
                },
                Command::Post(post_id) => {
                    if let Err(err) = on_post(&bot, &msg, post_id).await {
                        let _ = bot.send_message(msg.chat.id, "Ein Fehler ist aufgetreten.")
                            .reply_to_message_id(msg.id)
                            .await;
                        error!("Error on message: \"{}\"", msg_text);
                        error!("{:?}", err);
                    }
                },
                _ => {}
            }
        }
    }

    Ok(())
}

pub async fn on_callback_query(bot: Bot, callback_query: CallbackQuery) -> ResponseResult<()> {
    if let Some(data) = callback_query.data.clone() {
        if let Ok(event) = serde_json::from_str::<EventData>(data.as_str()) {
            match event {
                EventData::JoinToggle{ event_id } => {
                    if let Err(err) = toggle_user_join_event(&bot, &callback_query, event_id).await {
                        error!("JoinToggle: {}", err);
                    }
                }
                EventData::AddGuest{ event_id } => {
                    if let Err(err) = on_add_guest_to_event(&bot, &callback_query, event_id).await {
                        error!("AddGuest: An error occured while trying to add guest: {err}");
                        let _ = bot.answer_callback_query(callback_query.id.clone())
                            .text("Ein Fehler ist aufgetreten.");
                    }
                }
                EventData::RemoveGuest{ event_id } => {
                    if let Err(err) = on_remove_guest_from_event(&bot, &callback_query, event_id).await {
                        error!("RemoveGuest: {}", err);
                    }
                }
            }
        } else {
            warn!("Could not parse event data");
        }
    } else {
        warn!("No event data provided");
        let _ = bot.answer_callback_query(callback_query.id)
            .text("Unbekannter Befehl")
            .show_alert(true)
            .await?;
    }

    Ok(())
}

pub async fn toggle_user_join_event(bot: &Bot, callback_query: &CallbackQuery, event_id: String) -> anyhow::Result<()> {
    let client = pocketbase_init()?;
    let event_result = get_event_by_id(&client, &event_id).await;
    let internal_user_result = upsert_user(&client, &callback_query.from).await;

    if let Ok(internal_telegram_user) = internal_user_result {
        if let Ok(event) = event_result {
            if let Some(event_registration) = get_registration(&client, &event_id, callback_query.from.id).await? {
                if let Err(err) = delete_registration(&client, event_registration).await {
                    error!("Could not delete registration: {err}");
                    let _ = bot.answer_callback_query(callback_query.id.clone())
                        .text("Deine Registrierung könnte wegen einem Fehler nicht gelöscht werden")
                        .await?;
                } else {
                    let _ = bot.answer_callback_query(callback_query.id.clone())
                        .text("Deine Registrierung wurde gelöscht (dies beinhaltet deine Gäste)")
                        .await?;

                    if let Ok(registrations) = get_registations_by_event(&client, &event_id).await {
                        if let Some(msg) = &callback_query.message {
                            let _ = update_post(&bot, msg.chat.id, msg.id, event, registrations).await;
                        } else {
                            warn!("Could not get callback query message");
                        }
                    } else {
                        warn!("Could not get registrations.")
                    }
                }
            } else {
                if let Err(err) = create_registration(&client, internal_telegram_user, event_id.clone()).await {
                    error!("Could not register user: {err}");
                    let _ = bot.answer_callback_query(callback_query.id.clone())
                        .text("Fehler: Registrierung fehlgeschlagen.")
                        .await?;
                } else {
                    let _ = bot.answer_callback_query(callback_query.id.clone())
                        .text("Du wurdest erfolgreich registriert!")
                        .await?;

                    if let Ok(registrations) = get_registations_by_event(&client, &event_id).await {
                        if let Some(msg) = &callback_query.message {
                            let _ = update_post(&bot, msg.chat.id, msg.id, event, registrations).await;
                        } else {
                            warn!("Could not get callback query message");
                        }
                    } else {
                        warn!("Could not get registrations.")
                    }
               }
            }
        } else if let Err(_err) = event_result {
            error!("Could not find event which user tried to register for: {event_id}");
            let _ = bot.answer_callback_query(callback_query.id.clone())
                .text("Fehler: Das Event konnte nicht gefunden werden")
                .await?;
        }
    } else {
        error!("Could not fetch userdata for: {}", callback_query.id);
        let _ = bot.answer_callback_query(callback_query.id.clone())
            .text("Fehler: Die Benutzerdaten konnten nicht geladen werden")
            .await?;
    }

    Ok(())
}

pub async fn on_add_guest_to_event(bot: &Bot, callback_query: &CallbackQuery, event_id: String) -> anyhow::Result<()> {
    let client = pocketbase_init()?;

    if !is_slot_available(&client, &event_id).await? {
        let _ = bot.answer_callback_query(callback_query.id.clone())
            .text("Es sind keine Plätze mehr frei!")
            .await?;
        return Ok(())
    }

    if let Some(mut registration) = get_registration(&client, &event_id, callback_query.from.id).await? {
        registration.guest_count += 1;
        set_registration(&client, &registration.id, (&registration).into()).await?;
        let _ = bot.answer_callback_query(callback_query.id.clone())
            .text(format!("Du hast jetzt {} Gäste eingeladen.", registration.guest_count))
            .await?;
    } else {
        let _ = bot.answer_callback_query(callback_query.id.clone())
            .text("Du kannst nur Gäste hinzufügen wenn du registriert bist.")
            .await?;
    }

    if let Ok(registrations) = get_registations_by_event(&client, &event_id).await {
        if let Ok(event) = get_event_by_id(&client, &event_id).await {
            if let Some(msg) = &callback_query.message {
                let _ = update_post(&bot, msg.chat.id, msg.id, event, registrations).await;
            } else {
                warn!("Could not get callback query message");
            }
        } else {
            warn!("Could not get event");
        }
    } else {
        warn!("Could not get registrations.")
    }

    Ok(())
}

pub async fn on_remove_guest_from_event(bot: &Bot, callback_query: &CallbackQuery, event_id: String) -> anyhow::Result<()> {
    let client = pocketbase_init()?;
    if let Some(mut registration) = get_registration(&client, &event_id, callback_query.from.id).await? {
        if registration.guest_count != 0 {
            registration.guest_count -= 1;
            set_registration(&client, &registration.id, (&registration).into()).await?;
        }

        let _ = bot.answer_callback_query(callback_query.id.clone())
            .text(format!("Du hast jetzt {} Gäste eingeladen.", registration.guest_count))
            .await?;

        if let Ok(registrations) = get_registations_by_event(&client, &event_id).await {
            if let Ok(event) = get_event_by_id(&client, &event_id).await {
                if let Some(msg) = &callback_query.message {
                    let _ = update_post(&bot, msg.chat.id, msg.id, event, registrations).await;
                } else {
                    warn!("Could not get callback query message");
                }
            } else {
                warn!("Could not get event");
            }
        } else {
            warn!("Could not get registrations.")
        }
    } else {
        let _ = bot.answer_callback_query(callback_query.id.clone())
            .text("Du kannst nur Gäste entfernen wenn du registriert bist.")
            .await?;
    }
    Ok(())
}

pub async fn on_inline_query(bot: Bot, inline_query: InlineQuery) -> ResponseResult<()> {
    let _ = bot.answer_inline_query(inline_query.id, vec![]);
    Ok(())
}

pub async fn send_in_private<T>(bot: &Bot, msg: &Message, private_message: T) -> bool
    where T: Into<String> {
    let is_private = msg.chat.is_private();
    if is_private {
        let _ = bot.send_message(msg.chat.id, private_message).await;
    } else {
        let _ = bot.send_message(msg.chat.id, "Dieser Befehl kann hier nicht verwendet werden.").await;
    }
    is_private
}

pub async fn on_help(bot: &Bot, msg: &Message) {
    let _ = send_in_private(bot, msg, format!("TODO: Help\nUserID: {}", msg.from().unwrap().id)).await;
}

pub async fn on_dsgvo(bot: &Bot, msg: &Message) {
    let _ = send_in_private(bot, msg, "TODO: DSGVO").await;
}

pub async fn on_post(bot: &Bot, msg: &Message, event_id: String) -> anyhow::Result<()> {
    if let Some(msg_user) = msg.from() {
        if !user_is_admin(&msg_user.id) {
            let _ = bot.send_message(msg.chat.id, "Du hast keine Berechtigungen diesen Befehl auszuführen.")
                .reply_to_message_id(msg.id)
                .await;
            return Ok(());
        }

        let client = pocketbase_init()?;

        return match get_event_by_id(&client, &event_id).await {
            Ok(event) => {
                if let Some(last_message_id) = get_last_post_message_id(msg.chat.id)? {
                    if let Err(err) = bot.delete_message(msg.chat.id, last_message_id).await {
                        error!("Could not delete message before posting new post: {err}");
                    } else {
                        delete_last_post_message_id(msg.chat.id)?;
                    }
                }

                let post_msg = bot.send_message(msg.chat.id, get_post_text(event.clone(), None).await?)
                    .reply_markup(InlineKeyboardMarkup::new(
                        vec![
                            vec![
                                InlineKeyboardButton::callback("Teilname ändern", serde_json::to_string(&EventData::JoinToggle { event_id: event.id.clone() })?)
                            ],
                            vec![
                                InlineKeyboardButton::callback("+1 Gast", serde_json::to_string(&EventData::AddGuest { event_id: event.id.clone() })?),
                                InlineKeyboardButton::callback("-1 Gast", serde_json::to_string(&EventData::RemoveGuest { event_id: event.id.clone() })?)
                            ]
                        ]
                    )).await?;
                save_last_post_message_id(msg.chat.id, post_msg.id)?;

                if let Ok(event_registrations) = get_registations_by_event(&client, &event_id).await {
                    update_post(bot, msg.chat.id, post_msg.id, event, event_registrations).await?;
                } else {
                    warn!("Could not get registrations")
                }
                Ok(())
            },
            Err(err) => {
                let _ = bot.send_message(msg.chat.id, format!("Könnte das Event nicht finden."))
                    .reply_to_message_id(msg.id)
                    .await?;
                error!("An error occured trying to find the event {event_id}: {err}");
                Ok(())
            }
        }
    } else {
        warn!("Message did not contain user");
    }


    Ok(())
}