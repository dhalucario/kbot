use std::collections::HashMap;
use tinytemplate::TinyTemplate;
use std::fs::{read_dir, read_to_string};
use teloxide::Bot;
use teloxide::types::{ChatId, InlineKeyboardButton, InlineKeyboardMarkup, MessageId};
use crate::pb::{Event, EventRegistrationResult};
use serde::Serialize;
use teloxide::prelude::*;
use crate::common::EventData;


#[derive(Serialize)]
struct PostTeplateData {
    event_name: String,
    event_description: String,
    event_date: String,
    event_location: String,
    registration_list: String,
}

const TEMPLATES_PATH: &str = "./templates";
lazy_static! {
    static ref TEMPLATES: HashMap<String, String> = {
        let dirs = read_dir(TEMPLATES_PATH).expect("Could not read dir template");
        let mut files: HashMap<String, String> = Default::default();

        let paths: Vec<(String, String)> = dirs.filter_map(|dir_entry_res| {
            if let Ok(dir_entry) = dir_entry_res {
                let path = dir_entry.path();
                let path_string = path.to_string_lossy().to_string();
                if let Ok(file_type) = dir_entry.file_type() {
                    if file_type.is_file() && path_string.ends_with(".ttp") {
                        return Some((path_string, path.file_name().expect("Could not get filename").to_string_lossy().to_string().replace(".ttp", "")));
                    }
                }
            }
            return None;
        }).collect();

        for (path, name) in paths {
            files.insert(name, read_to_string(path.clone()).expect(&format!("Could not load template {path}")));
        }

        files
    };

}

pub fn load_templates() -> anyhow::Result<TinyTemplate<'static>> {
    let mut tiny_template = TinyTemplate::new();
    for (file_name, content) in TEMPLATES.iter() {
        tiny_template.add_template(&file_name, content)?;
    }
    Ok(tiny_template)
}

pub async fn get_post_text(event: Event, event_registrations_option: Option<Vec<EventRegistrationResult>>) -> anyhow::Result<String> {
    let template = load_templates()?;
    let mut registration_list;
    if let Some(event_registrations) = event_registrations_option {
        registration_list = String::from("Anwesenheitsliste:\n");
        for event_registration in event_registrations {
            registration_list.push_str(&event_registration.user);
            if event_registration.guest_count > 0 {
                registration_list.push_str(&format!(" (+{} Gast)", event_registration.guest_count))
            }
        }
    } else {
        registration_list = String::from("Laden...");
    }

    let res = template.render("post", &PostTeplateData {
        event_name: event.name,
        event_description: event.description,
        event_date: event.date,
        event_location: event.location,
        registration_list
    })?;
    Ok(res)
}

pub async fn update_post(bot: &Bot, chat_id: ChatId, message_id: MessageId, event: Event, event_registrations: Vec<EventRegistrationResult>) -> anyhow::Result<()> {
    let _ = bot
        .edit_message_text(chat_id, message_id, get_post_text(event.clone(), Some(event_registrations)).await?)
        .reply_markup(InlineKeyboardMarkup::new(
            vec![
                vec![
                    InlineKeyboardButton::callback("Teilname ändern", serde_json::to_string(&EventData::JoinToggle { event_id: event.id.clone() })?)
                ],
                vec![
                    InlineKeyboardButton::callback("+1 Gast", serde_json::to_string(&EventData::AddGuest { event_id: event.id.clone() })?),
                    InlineKeyboardButton::callback("-1 Gast", serde_json::to_string(&EventData::RemoveGuest { event_id: event.id.clone() })?)
                ]
            ]
        ))
        .await?;
    Ok(())
}
