use pocketbase_sdk::client::{Auth, Client};
use serde::{Serialize, Deserialize};
use teloxide::types::{User, UserId};

const POCKETBASE_URL: &str = dotenv!("POCKETBASE_URL");
const POCKETBASE_USER: &str = dotenv!("POCKETBASE_USER");
const POCKETBASE_PASS: &str = dotenv!("POCKETBASE_PASS");

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Event {
    pub id: String,
    pub created: String,
    pub updated: String,
    pub name: String,
    pub slug: String,
    pub description: String,
    pub date: String,
    pub location: String,
    pub max_attendees: u32
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct TelegramUserResult {
    pub id: String,
    pub telegram_id: u64,
    pub first_name: String,
    pub last_name: Option<String>,
    pub username: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct NewTelegramUser {
    pub telegram_id: u64,
    pub first_name: String,
    pub last_name: Option<String>,
    pub username: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct EventRegistrationResult {
    pub id: String,
    pub event: String,
    pub user: String,
    pub guest_count: u32
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct NewEventRegistration {
    pub event: String,
    pub user: String,
    pub guest_count: u32
}

impl Into<NewEventRegistration> for EventRegistrationResult {
    fn into(self) -> NewEventRegistration {
        NewEventRegistration {
            user: self.user,
            event: self.event,
            guest_count: self.guest_count
        }
    }
}

impl Into<NewEventRegistration> for &EventRegistrationResult {
    fn into(self) -> NewEventRegistration {
        NewEventRegistration {
            user: self.user.clone(),
            event: self.event.clone(),
            guest_count: self.guest_count.clone()
        }
    }
}

pub fn pocketbase_init() -> anyhow::Result<Client<Auth>> {
    Client::new(POCKETBASE_URL)
        .auth_with_password("users", POCKETBASE_USER, POCKETBASE_PASS)
}

pub async fn is_slot_available(client: &Client<Auth>, event_id: &String) -> anyhow::Result<bool> {
    Ok(get_max_attendee_count(client, event_id).await? > get_current_attendee_count(client, event_id).await?)
}

pub async fn get_max_attendee_count(client: &Client<Auth>, event_id: &String) -> anyhow::Result<u32>{
    let event = get_event_by_id(client, event_id).await?;
    Ok(event.max_attendees)
}

pub async fn get_current_attendee_count(client: &Client<Auth>, event_id: &String) -> anyhow::Result<u32> {
    let event_registations = client.records("event_registrations")
        .list()
        .filter(&format!("event.id = \"{event_id}\""))
        .call::<EventRegistrationResult>()?;

    Ok(event_registations.items.into_iter()
        .fold(0 as u32, |accumulator, event_registration| {
            accumulator+1+event_registration.guest_count
        }))
}

pub async fn upsert_user(client: &Client<Auth>, request_user: &User) -> anyhow::Result<TelegramUserResult> {
    let pocketbase_users = client
        .records("telegram_users")
        .list()
        .filter(&format!("telegram_id = {}", request_user.id))
        .call::<TelegramUserResult>()?;

    let current_user;

    if let Some(telegram_user) = pocketbase_users.items.first() {
        current_user = telegram_user.to_owned();
    } else {
        let create_response = client.records("telegram_users")
            .create(NewTelegramUser {
                telegram_id: request_user.id.0,
                first_name: request_user.first_name.clone(),
                last_name: request_user.last_name.clone(),
                username: request_user.username.clone(),
            })
            .call()?;

        current_user = TelegramUserResult {
            id: create_response.id,
            telegram_id: request_user.id.0,
            first_name: request_user.first_name.to_owned(),
            last_name: request_user.last_name.to_owned(),
            username: request_user.username.to_owned()
        }
    }

    Ok(current_user)
}

pub async fn get_event_by_id(client: &Client<Auth>, event_id: &String) -> anyhow::Result<Event> {
    client.records("events")
        .view(event_id.as_str())
        .call::<Event>()
}

pub async fn create_registration(client: &Client<Auth>, internal_telegram_user: TelegramUserResult, event_id: String) -> anyhow::Result<EventRegistrationResult> {
    let res = client.records("event_registrations")
        .create(NewEventRegistration {
            event: event_id.clone(),
            user: internal_telegram_user.id.clone(),
            guest_count: 0
        })
        .call()?;

    Ok(EventRegistrationResult {
        id: res.id,
        event: event_id,
        user: internal_telegram_user.id,
        guest_count: 0
    })
}

pub async fn delete_registration(client: &Client<Auth>, registration: EventRegistrationResult) -> anyhow::Result<()> {
    client
        .records("event_registrations")
        .destroy(&registration.id)
        .call()
}

pub async fn get_registration(client: &Client<Auth>, event_id: &String, telegram_id: UserId) -> anyhow::Result<Option<EventRegistrationResult>> {
    let event_registrations = client.records("event_registrations")
        .list()
        .filter(&format!("event.id = \"{event_id}\" && user.telegram_id = {telegram_id}"))
        .call::<EventRegistrationResult>()?;

    if let Some(event_registration) = event_registrations.items.first() {
        return Ok(Some(event_registration.to_owned()));
    }

    Ok(None)
}

pub async fn set_registration(client: &Client<Auth>, registation_id: &String, new_registration: NewEventRegistration) -> anyhow::Result<()> {
    client.records("event_registrations")
        .update(registation_id, new_registration)
        .call()?;
    Ok(())
}

pub async fn get_registations_by_event(client: &Client<Auth>, event_id: &String) -> anyhow::Result<Vec<EventRegistrationResult>> {
    Ok(client.records("event_registrations")
        .list()
        .filter(&format!("event.id = \"{event_id}\""))
        .call::<EventRegistrationResult>()?.items)
}