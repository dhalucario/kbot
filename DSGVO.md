# Datenschutzerklärung

Um eine faire und nachvollziehbare Datenverarbeitung zu gewährleisten,
beinhaltet diese Datenschutzerklärung allgemeine Angaben mit dem Umgang der Daten
und Informationen zur Europäischen Datenschutz-Grundverordnung (DSGVO)
und dem Bundesdatenschutzgesetz (BDSG).

## 1. Kontakt

Verantwortlich für die Datenverarbeitung sind die Veranstalter des Kölner Furry Stammtischs.
Wenn ihr fragen oder Anregungen zu diesen Informationen habt oder euch wegen einer Geltendmachung eurer Rechte
an uns wenden wollt, richtet die Anfrage bitte an Leon ([@yesthisiscario](https://t.me/yesthisiscario)) auf Telegram.

## 2. Verarbeitung und Dauer der Speicherung

### Altersnachweis
Die Telegram ID und der Altersnachweis werden zur vereinfachung von zukünftigen Registrierungen
bis zur löschanfrage gespeichert.

### Registrierungen
Registrierungen beinhalten die ID, Vorname und Nachname von Telegram und
werden nach dem Event zum nächstmöglichen Zeitpunkt gelöscht.

### Debugging
Falls es dazu kommen sollte das der Bot Fehler bei der Datenverarbeitung erzeugt, halten wir es uns vor das diese
in einem Log, zur Fehlerbehebung, für bis zu 2 Wochen speichern.

## 3. Externe Dienstleister
Zum Zeitpunkt der verfassung dieses Textes werden die Daten von folgenden Dienstleistern verarbeitet:  
Als Basis des Bots wird die [Telegram Bot API](https://core.telegram.org/bots/api) verwendet.
Wie die dabei anfallenden Daten von Telegram werden, könnt ihr in der [Datenschutzerklärung von Telegram](https://telegram.org/privacy) einsehen.
Der Bot und die Datenbank werden auf einem Server von Hetzner in [Datacenter-Park Falkenstein, Deutschland](https://www.hetzner.com/de/unternehmen/rechenzentrum/)
verarbeitet und gespeichert.

## 4. Rechte
Als betroffene Person haben Ihr das Recht, uns gegenüber eure Betroffenenrechte geltend zu machen.
Dabei haben Ihr insbesondere die folgenden Rechte:

### Gesetzesgrundlagen

- Ihr habt nach Maßgabe des Art.15 DSGVO und § 34 BDSG das Recht, Auskunft darüber zu verlangen,
ob und gegebenenfalls in welchen Umfang wir personenbezogene Daten zu Ihrer Person verarbeiten oder nicht.

- Ihr habt das Recht, nach Maßgabe des Art. 16 DSGVO von uns die Berichtigung Ihrer Daten zu verlangen.

- Ihr habt das Recht, nach Maßgabe des Art. 17 DSGVO und § 35 BDSG von uns 
die Löschung Ihrer personenbezogenen Daten zu verlangen.

- Ihr habt das Recht, nach Maßgabe des Art. 18 DSGVO die Verarbeitung eurer
personenbezogenen Daten einschränken zu lassen.

- Ihr habt das Recht, nach Maßgabe des Art. 20 DSGVO die Sie betreffenden personenbezogenen Daten,
die Ihr uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten,
und diese Daten einem anderen Verantwortlichen zu übermitteln.

- Ihr habt nach Maßgabe des Art. 21 Abs. 1 DSGVO das Recht, gegen jede Verarbeitung,
die auf der Rechtsgrundlage des Art. 6 Abs. 1 Buchst. e) oder f) DSGVO beruht, Widerspruch einzulegen. 
Sofern durch uns personenbezogene Daten über Euch zum Zweck der Direktwerbung verarbeitet werden,
könnt Ihr gegen diese Verarbeitung gem. Art. 21 Abs. 2 und Abs. 3 DSGVO Widerspruch einlegen.

Sofern Ihr uns eine gesonderte Einwilligung in die Datenverarbeitung erteilt habt,
könnt Ihr diese Einwilligung nach Maßgabe des Art. 7 Abs. 3 DSGVO jederzeit widerrufen.
Durch einen solchen Widerruf wird die Rechtmäßigkeit der Verarbeitung,
die bis zum Widerruf aufgrund der Einwilligung erfolgt ist, nicht berührt.

### Beschwerde bei einer Aufsichtsbehörde

Wenn Ihr der Ansicht seid, dass eine Verarbeitung der Sie betreffenden personenbezogenen Daten
gegen die Bestimmungen der DSGVO verstößt, haben Ihr nach Maßgabe des Art. 77 DSGVO
das Recht auf Beschwerde bei einer Aufsichtsbehörde.